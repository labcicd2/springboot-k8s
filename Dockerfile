FROM openjdk:17-jdk-slim

LABEL maintainer = "Boubacar Siddy DIALLO boubasiddy00@gmail.com"

EXPOSE 8084

ADD target/springboot-k8s-0.0.1.jar springboot-k8s.jar

ENTRYPOINT ["java", "-jar", "springboot-k8s.jar"]