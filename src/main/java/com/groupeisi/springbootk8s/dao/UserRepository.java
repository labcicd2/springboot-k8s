package com.groupeisi.springbootk8s.dao;

import com.groupeisi.springbootk8s.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
}
