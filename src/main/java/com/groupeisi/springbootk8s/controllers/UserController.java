package com.groupeisi.springbootk8s.controllers;

import com.groupeisi.springbootk8s.entity.User;
import com.groupeisi.springbootk8s.service.UserService;
import jakarta.validation.Valid;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public List<User> getUsers(){
        return userService.getUsers();
    }
    @GetMapping("/{id}")
    public User getById(@PathVariable("id") int id){
        return userService.getUserById(id);
    }
    @PostMapping
    public User save(@Valid @RequestBody User user){
        return userService.save(user);
    }
    @PutMapping("/{id}")
    public User update(@PathVariable("id") int id, @RequestBody User user){
        return userService.update(id, user);
    }
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") int id){
        userService.delete(id);
    }
}
