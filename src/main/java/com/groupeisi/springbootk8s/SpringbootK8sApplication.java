package com.groupeisi.springbootk8s;

//import com.groupeisi.springbootk8s.entity.User;
//import com.groupeisi.springbootk8s.service.UserService;
//import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringbootK8sApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootK8sApplication.class, args);
	}
//	@Bean
//	CommandLineRunner commandLineRunner(UserService userService){
//		return args -> {
//			userService.save(User.builder().name("Diallo").lastName("Boubacar Siddy").email("diallo@gmail.com").phoneNumber("451255544").build());
//			userService.save(User.builder().name("Diallo").lastName("Ousmane").email("ousmane@gmail.com").phoneNumber("7777777").build());
//			userService.save(User.builder().name("Diallo").lastName("Abdoullah").email("abd@gmail.com").phoneNumber("78888888").build());
//		};
//	}

}
