package com.groupeisi.springbootk8s.service;

import com.groupeisi.springbootk8s.dao.UserRepository;
import com.groupeisi.springbootk8s.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;
    Logger logger = LoggerFactory.getLogger(UserService.class);

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Transactional(readOnly = true)
    public List<User> getUsers(){
        return userRepository.findAll();
    }
    @Transactional(readOnly = true)
    public User getUserById(int id){
        return userRepository.findById(id).orElseThrow();
    }
    @Transactional
    public User save(User user){
        return userRepository.save(user);
    }

    @Transactional
    public User update(int id, User user){
        return userRepository.findById(id).map(entity -> {
            user.setId(id);
            return userRepository.save(user);
        }).orElseThrow();
    }

    public void delete(int id){
        try{
            userRepository.deleteById(id);
        }catch (Exception e){
            logger.warn("Erreur de suppression de l'utilisateur avec l'id {}", id);
        }
    }
}
